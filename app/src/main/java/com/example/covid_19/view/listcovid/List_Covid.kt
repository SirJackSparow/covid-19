package com.example.covid_19.view.listcovid


import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.app.ActivityOptionsCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.covid_19.R
import com.example.covid_19.data.model.Attributes
import com.example.covid_19.viewfactory.CovidFactory
import com.example.covid_19.viewmodel.CovidViewModel
import kotlinx.android.synthetic.main.fragment_list__covid.*
import kotlinx.coroutines.launch


/**
 * A simple [Fragment] subclass.
 */
class List_Covid : Fragment() {

    private lateinit var adapters: AdapterCovid

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for this fragment
        return inflater.inflate(
            R.layout.fragment_list__covid,
            container,
            false
        )
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val factory = CovidFactory.getInstance(context!!)
        val viewModel = ViewModelProvider(this, factory)[CovidViewModel::class.java]
        //(activity as AppCompatActivity).setSupportActionBar(tolbar)

        //ubah status bar jadi hijau bar paling atas
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity?.window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            activity?.window?.statusBarColor = resources.getColor(R.color.biru)
        }

        var data: MutableList<Attributes> = mutableListOf()
        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                adapters.searchingData(s.toString())
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        viewLifecycleOwner.lifecycleScope.launch {

            //jika tidak pakai coroutines tinggal ganti viewLifecycleOwner dengan this
            viewModel.getDataCovid().observe(viewLifecycleOwner, Observer {
                for (x in 0 until it.size) {
                    data.add(it[x].attributes)
                }

                adapters = AdapterCovid(data) { data ->

                    Log.e("Erno", "${data.Confirmed}, ${data.Deaths}")
                    val bundle = bundleOf(
                        "country" to data.Country_Region,
                        "confirm" to data.Confirmed,
                        "die" to data.Deaths,
                        "recovered" to data.Recovered,
                        "active" to data.Active,
                        "lastupdate" to data.Last_Update
                    )

                    findNavController().navigate(R.id.action_list_Covid_to_detailCovid19, bundle)
                }

                data_covid19.apply {
                    layoutManager = LinearLayoutManager(context)
                    adapter = adapters

                }

                loading.visibility = View.GONE
                data_covid19.visibility = View.VISIBLE
                search.isEnabled = true

            })
        }


    }


}
