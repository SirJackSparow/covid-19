package com.example.covid_19.view

import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.covid_19.R
import kotlinx.android.synthetic.main.fragment_detail_covid19.*
import java.text.DecimalFormat


class DetailCovid19 : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail_covid19, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val formatter = DecimalFormat("#,###,###")

        countries.text = arguments?.getString("country")
        confirm_tv.text = formatter.format(arguments?.getInt("confirm",0)).toString()
        die.text = formatter.format(arguments?.getInt("die",0)).toString() + "\nDeath"
        sick.text = formatter.format(arguments?.getInt("active",0)).toString() + "\nSick"
        recover.text = formatter.format(arguments?.getInt("recovered",0)).toString() + "\nrecovered"


    }
}
