package com.example.covid_19.view.listcovid

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.example.covid_19.R
import com.example.covid_19.data.model.Attributes
import java.text.DecimalFormat


class AdapterCovid(val data: MutableList<Attributes>, val listener: (Attributes) -> Unit) :
    RecyclerView.Adapter<ViewHolder>() {

    val datax: MutableList<Attributes> = mutableListOf()

    init {
        datax.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_covid, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = data.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val datax = data[position]

        ViewCompat.setTransitionName(holder.images, "location")

        holder.country.text = datax.Country_Region
        val formatter = DecimalFormat("#,###,###")
        val jumlah = formatter.format(datax.Confirmed)
        holder.confirmed.text = "$jumlah People"

        holder.button.setOnClickListener {
            listener(datax)
        }

    }

    fun searchingData(searching: String) {
        data.clear()
        if (searching.trim().isNotEmpty()) {
            data.addAll(datax.filter { s ->
                s.Country_Region.toLowerCase()
                    .contains(searching.toLowerCase())
            })
            notifyDataSetChanged()

        } else {
            data.addAll(datax)
            notifyDataSetChanged()
        }

    }
}


class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val confirmed: TextView = itemView.findViewById(R.id.confirm)
    val country: TextView = itemView.findViewById(R.id.country)
    val button: Button = itemView.findViewById(R.id.details)

    val images: LottieAnimationView = itemView.findViewById(R.id.images)


}
