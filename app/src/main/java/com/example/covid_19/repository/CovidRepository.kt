package com.example.covid_19.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.covid_19.data.model.CovidModels
import com.example.covid_19.data.network.CovidDataSources
import com.example.covid_19.data.network.RemoteDataInterface

class CovidRepository(
    val remoteDataSources: CovidDataSources
) {
    companion object {
        @Volatile
        private var instance: CovidRepository? = null

        fun getInstance(remoteDataSources: CovidDataSources): CovidRepository =
            instance ?: synchronized(this) {
                instance ?: CovidRepository(remoteDataSources)
            }
    }

    fun getCovidData(): LiveData<CovidModels> {
        val datax = MutableLiveData<CovidModels>()
        remoteDataSources.getCovidData(object : RemoteDataInterface {
            override fun data(data: CovidModels?) {
                datax.value = data
            }

        })
        return datax
    }

   suspend fun getCovidData2(): LiveData<CovidModels> {
      val datax = MutableLiveData<CovidModels>()
       remoteDataSources.getCovidDatas(object : RemoteDataInterface{
           override fun data(data: CovidModels?) {
               datax.postValue(data)
           }

       })

       return datax
    }

}