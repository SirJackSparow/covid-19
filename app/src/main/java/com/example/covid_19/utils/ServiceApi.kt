package com.example.covid_19.utils

import com.example.covid_19.data.network.Services
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ServicesApi {
    companion object {
        fun retrofit () : Retrofit{
            var retrofit = Retrofit.Builder()
                .baseUrl( "https://api.kawalcorona.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit
        }

        fun retrofit2 () : Services {
            var retrofit = Retrofit.Builder()
                .baseUrl( "https://api.kawalcorona.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(Services::class.java)
            return retrofit
        }

    }
}