package com.example.covid_19.data.network

import com.example.covid_19.data.model.CovidModels

interface RemoteDataInterface {
    fun data(data: CovidModels?)
}