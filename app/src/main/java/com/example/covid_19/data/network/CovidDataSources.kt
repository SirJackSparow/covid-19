package com.example.covid_19.data.network

import android.util.Log
import com.example.covid_19.data.model.CovidModels
import com.example.covid_19.utils.ServicesApi
import com.example.covid_19.utils.ServicesApi.Companion.retrofit2
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CovidDataSources {

    companion object {
        @Volatile
        private var instance: CovidDataSources? = null

        fun getInstance(): CovidDataSources = instance ?: synchronized(this) {
            instance ?: CovidDataSources()
        }
    }


    fun getCovidData(remote: RemoteDataInterface) {
        val retrofits = ServicesApi.retrofit().create(Services::class.java)
        val getData = retrofits.getData()

        getData.enqueue(object : Callback<CovidModels> {
            override fun onFailure(call: Call<CovidModels>, t: Throwable) {
                Log.e("Error Covid", t.message)
            }

            override fun onResponse(call: Call<CovidModels>, response: Response<CovidModels>) {
                remote.data(response.body())
            }

        })
    }

    suspend fun getCovidDatas(remote: RemoteDataInterface) {
        withContext(Dispatchers.IO){
            val response = retrofit2().getDatas()

            if (response.isSuccessful) {

                remote.data(response.body())
            }else {
                remote.data(null)
            }
        }


    }
}