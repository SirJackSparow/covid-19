package com.example.covid_19.data.network


import com.example.covid_19.data.model.CovidModels
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.GET

interface Services {

    @GET("/")
    fun getData(): Call<CovidModels>

    @GET("/")
    suspend fun getDatas() : Response<CovidModels>

}