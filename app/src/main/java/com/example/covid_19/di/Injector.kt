package com.example.covid_19.di

import android.content.Context
import com.example.covid_19.data.network.CovidDataSources
import com.example.covid_19.repository.CovidRepository

object Injector {

    fun CovidInject(context: Context): CovidRepository {

        val remoteData = CovidDataSources.getInstance()
        return CovidRepository.getInstance(remoteData)

    }
}