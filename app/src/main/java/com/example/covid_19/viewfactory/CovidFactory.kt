package com.example.covid_19.viewfactory

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.covid_19.di.Injector
import com.example.covid_19.repository.CovidRepository
import com.example.covid_19.viewmodel.CovidViewModel

class CovidFactory(val mRepository: CovidRepository) : ViewModelProvider.NewInstanceFactory() {

    companion object {
        @Volatile
        private var instance: CovidFactory? = null

        fun getInstance(context: Context): CovidFactory =
            instance ?: synchronized(this) {
                instance ?: CovidFactory(Injector.CovidInject(context))
            }
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(CovidViewModel::class.java) -> {
                CovidViewModel(mRepository) as T
            }
            else -> throw Throwable("Unknown ViewModel class: " + modelClass.name)
        }

    }
}