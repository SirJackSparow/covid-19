package com.example.covid_19.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.covid_19.data.model.CovidModels
import com.example.covid_19.repository.CovidRepository

class CovidViewModel(val repository: CovidRepository) : ViewModel() {

    suspend fun getDataCovid() : LiveData<CovidModels>{
      return repository.getCovidData2()
    }
}